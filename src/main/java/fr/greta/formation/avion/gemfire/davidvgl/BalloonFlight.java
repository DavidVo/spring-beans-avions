package fr.greta.formation.avion.gemfire.davidvgl;

import javax.persistence.Column;
import javax.persistence.Entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.gemfire.mapping.annotation.Region;

@Region("balloonRegion")
@Entity
public class BalloonFlight {
	
	private Integer id;
	private String name;
	private double size;
	private double altitude;
	
	//ici prendre l'annotation de spring.data, c'est un id donc on indique Id
	@Id
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	//ici on pourrait mettre en plus des option après le Column
	@Column
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Column
	public double getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	
	@Column
	public double getAltitude() {
		return altitude;
	}
	
	public void setAltitude(double altitude) {
		this.altitude = altitude;
	}
	
	public BalloonFlight(int i, String na, double si, double al) {
		id=i;
		name=na;
		size=si;
		altitude=al;
	}
	
	public BalloonFlight() {
		this(0,"na",0.0,0.0);
	}
	
	
	@Override
	public String toString() {
		return "BalloonFlight [id=" + id + ", name=" + name + ", size=" + size + ", altitide=" + altitude + "]";
		
	}
	
	/**
	public String toASCII() {
		int taille = 2;
		String debut = "/";
		String fin = "\\";
		for(int i; i< 3; i++){
			debut += " ";
			}
		String finale = debut+fin;
		return finale;
	}
	**/


}
