package fr.greta.formation.avion.gemfire.davidvgl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.gemfire.repository.config.EnableGemfireRepositories;
import org.springframework.stereotype.Service;

@EnableGemfireRepositories("fr.greta.formation.avion.gemfire")
@Service("balloonService")
public class BalloonService {
	
	private BalloonRepository repo;
	
	public BalloonRepository getRepo() {
		return repo;
	}
	
	@Autowired
	public void setRepo(BalloonRepository repo) {
		this.repo = repo;
	}



	public void execute() {
		System.out.println("99 luftBallons");
		
		repo.save(new BalloonFlight(1937, "Hindenburg", 25, 0));
		repo.save(new BalloonFlight(1432, "Mongolfier2", 40, 10000));
		repo.save(new BalloonFlight(1987, "Rond", 125, 250));
		repo.save(new BalloonFlight(2019, "Bonjour", 22,850));
		
		
		// Pour modifier l'altitude te toutes les mongolfières ici j'ai mis 1 pour ne rien modifier 
		for(BalloonFlight al:repo.findAll()) {
			al.setAltitude(al.getAltitude()/1);
			repo.save(al);
		}
		
		// pour afficher tout les ballon
		for(BalloonFlight m:repo.findAll()) {
			System.out.println(m);
		}
		
		// pour afficher que les balllon suivant la methode créer dans BallooRepository
		for(BalloonFlight si:repo.findBySizeGreaterThanOrderByName(10)) {
			System.out.println(si);
		}
		
		// pour afficher que les balllon suivant la methode créer dans BallooRepository
		for(BalloonFlight si:repo.findByAltitudeGreaterThanAndAltitudeLessThanOrderBySize(100,500)) {
			System.out.println(si);
		}

		
	}
	

}
