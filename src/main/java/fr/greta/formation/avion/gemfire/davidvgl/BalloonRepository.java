package fr.greta.formation.avion.gemfire.davidvgl;

import org.springframework.data.repository.CrudRepository;

public interface BalloonRepository extends CrudRepository<BalloonFlight, Integer>{
	
	// Pour créer une method faire de la manière suivante ave iterable ... 
	// puis expliceter ce que l'on veut comme en sql
	Iterable<BalloonFlight> findByName(String n);
	Iterable<BalloonFlight> findBySizeGreaterThanOrderByName(double s);
	Iterable<BalloonFlight> findByAltitudeGreaterThanAndAltitudeLessThanOrderBySize(double s, double h); 
}
