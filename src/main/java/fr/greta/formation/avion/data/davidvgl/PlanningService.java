package fr.greta.formation.avion.data.davidvgl;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import fr.greta.formation.avion.davidvgl.Flight;

public class PlanningService {
	
	private PlanningDAO dao;
	
	public void setDao(PlanningDAO da) {
		this.dao= da;
	}
	
	public PlanningDAO getDao() {
		return dao;
	}
	
	public void execute() {
		System.out.println("\n");
		
		LocalDateTime dateNow = LocalDateTime.now(); 
		DateTimeFormatter formatterJour = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		DateTimeFormatter formatterHeure = DateTimeFormatter.ofPattern("HH:mm");
		
		String jourFormatted = dateNow.format(formatterJour);
		String heureFormatted = dateNow.format(formatterHeure);
		
		System.out.println("WELCOME, today is " + jourFormatted + " and it\'s " + heureFormatted);
		System.out.println("Our next scheduled flight is: ");
		
		
		long plane1 = dao.inserePlane("Airbus", "A380");
		long plane2 = dao.inserePlane("Airbus", "A350");
		long plane3 = dao.inserePlane("Airbus", "A330");
		long plane4 = dao.inserePlane("BAC & Airbus", "Concorde");
		
		dao.insereFlight(new Flight(LocalDateTime.of(1987, 02,26,9,00),"TLS","MRL",plane1));
		dao.insereFlight(new Flight(LocalDateTime.of(1954, 07,25,10,00),"DUB","ADL",plane2));
		dao.insereFlight(new Flight(LocalDateTime.of(1956, 01,15,10,30),"CDG","TLS",plane3));
		dao.insereFlight(new Flight(LocalDateTime.of(1981, 10,25,11,22),"ADL","CDG",plane4));
		
		System.out.println(dao.getFlightsByOrigin("TLS"));
		
		
		dao.addJourney("A380", "DXB", LocalDateTime.now(),LocalDateTime.now().plusWeeks(2));
	}
	
}
