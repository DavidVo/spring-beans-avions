package fr.greta.formation.avion.data.davidvgl;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;

import fr.greta.formation.avion.davidvgl.Flight;

public class PlanningDAO{

	private NamedParameterJdbcTemplate template;
	private final static String ownCode="TLS";

	public void setTemplate(NamedParameterJdbcTemplate t) {
		this.template= t;
	}
	
	public NamedParameterJdbcTemplate getTemplate() {
		return template;
	}
	
	public void initErase() {
		String delFlight = "DELETE FROM flight";
		String delPlane = "DELETE FROM plane";
		
	    //Pour se connecter via le jbctemplate la connection via notre "template" doit être accompagné des .getJdbcOperantion and ...
		template.getJdbcOperations().execute(delFlight);
		template.getJdbcOperations().execute(delPlane);
	}
	
	//Pour renvoyé la key créer par la BD pour nos planes on renvoie un long au cas ou la BD serait tres longue
	public long inserePlane(String model, String immat) {
		
		// le format dans VALUES :paramètre permet dan ce cas de remplir le BD grace aux valeur inerées dans la map
		String addPlane = "INSERT INTO plane(model, immat) VALUES (:model,:immat)";
		
		// Creation de la map sous cette forme pour pouvoir recuperer la cle primaire creer dans la bd
		MapSqlParameterSource mapPlane = new MapSqlParameterSource();
		mapPlane.addValue("model", model);
		mapPlane.addValue("immat", immat);
		
		// Si on avait pas à recup l'id on aurait pu Map<String, String> mapPlane = new HashMap<String, String>(); 
		// puis ajouté dans la map de la même manière avec add à la palce de addValue
		
		// h est un conteneur temporaire qu'on fourni à la fonction qui va la remplir par le fonction
		GeneratedKeyHolder h = new GeneratedKeyHolder();
		
		// Envoie et rempli h
		template.update(addPlane,mapPlane,h);
		
		return h.getKey().longValue();	
	}
	
	public void insereFlight(Flight f) {
		String addFlight = "INSERT INTO flight(id, departure, origin, destination, plane) "
				+ "VALUES (:id, :departure, :origin, :destination, :plane)";
		
		MapSqlParameterSource mapFlight = new MapSqlParameterSource();
		mapFlight.addValue("id", f.getId());
		mapFlight.addValue("departure", f.getDate());
		mapFlight.addValue("origin", f.getOrigin());
		mapFlight.addValue("destination", f.getDestination());
		mapFlight.addValue("plane", f.getPlane());
		
		template.update(addFlight,mapFlight);
	}
	
	public List<Flight> getFlightsByOrigin(String origin) {
		
		String selFlights = "SELECT * FROM flight WHERE origin LIKE :origin";
		
		MapSqlParameterSource mapSelFlight = new MapSqlParameterSource();
		mapSelFlight.addValue("origin", origin);
		
		// Création de la liste vide en prévision de stocker les données
		List<Flight> res = new ArrayList<Flight>();
		
		// Manière complexe pour l'id et plane, vu que la database revoie un intger et pas un LOng il faut d'abord caster m.get("id")
		// et m.get"plane") avec Integer, puis retransformer ces type en Long pour que ca colle avec notre definition de id et plande de notre classe 
		// flight. Meme diée pour departure qui envoie une erreur Timestamp, procedure pour d'abord caster en Timestamp puis changer en LocaDateTime
		for(Map<String,Object> m:template.queryForList(selFlights,mapSelFlight)) {
			res.add(new Flight(new Long((Integer) m.get("id")), ((Timestamp) m.get("departure")).toLocalDateTime(), 
					(String) m.get("origin"), (String) m.get("destination"), new Long ((Integer) m.get("plane"))));
		}
		
		return res;
	}
	
	
	public void addJourney(String immat, String destination, LocalDateTime dep1, LocalDateTime dep2) {
		
		String idPlane= "SELECT id FROM plane WHERE immat LIKE :immat";
		
		MapSqlParameterSource mapIdPlane = new MapSqlParameterSource();
		mapIdPlane.addValue("immat", immat);
		
		//Query for object permet de recuperer l'objet unique au format 
		Long IdPlane = template.queryForObject(idPlane, mapIdPlane, Long.class);
		
		// Aller
		insereFlight(new Flight(dep1,ownCode,destination,IdPlane));
		
		//Retour
		
		insereFlight(new Flight(dep2,destination,ownCode,IdPlane));
	
	}

}
