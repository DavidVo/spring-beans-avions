package fr.greta.formation.avion.davidvgl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.time.LocalDateTime;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.core.io.Resource;

import fr.greta.formation.avion.data.davidvgl.PlanningService;
import fr.greta.formation.avion.gemfire.davidvgl.BalloonService;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("");

		@SuppressWarnings("resource")
		AbstractApplicationContext context = new FileSystemXmlApplicationContext("classpath:context.xml");

		Resource ad = context.getResource("classpath:ad.txt");

		try {

			File adfile = ad.getFile(); // permet d'importer un fichier
			BufferedReader r = new BufferedReader(new FileReader(adfile)); // permet de lire par ligne et non par
																			// caractère
			String l;
			while ((l = r.readLine()) != null) { // boucle qui continue tant qu'une ligne contient quelquechose
				System.out.print(l + "\n");
			}

			/*
			 * methode classique sans Spring Coordinate c1 = new Coordinate(0,0,20);
			 * System.out.println(c1.toString()); //System.out.println(c1); //en ayant
			 * mit @override avec le methode apl toString on peut transformer en chaine et
			 * //apl c1 en codant de cette manière
			 */

			/**
			 * // Methode avec Spring Coordinate c1 = (Coordinate)
			 * context.getBean("towerCoord"); //System.out.println(c1 + ".\n|");
			 * 
			 * String aeroName = (String) context.getBean("aeroportname");
			 * //System.out.println(String.format("Dans la ville de %s.\n|", aeroName));
			 * 
			 * Integer hangSize = (Integer) context.getBean("hangarsize");
			 * //System.out.println(String.format("Il a une surface de %dm².\n|",
			 * hangSize));
			 * 
			 * Coordinate runCoord = (Coordinate) context.getBean("runwayCoord");
			 * //System.out.println(runCoord+ ".\n|");
			 * 
			 * Storage Stor1 = (Storage) context.getBean("hangars");
			 * //System.out.println(Stor1);
			 * 
			 * Plane plane1 = (Plane) context.getBean("plane1");
			 * //System.out.println(plane1);
			 * 
			 * LocalDateTime fond1 = (LocalDateTime) context.getBean("fondation");
			 * //System.out.println("l\'aérodrome a été rée le " + fond1.getDayOfMonth() +
			 * "-" + fond1.getMonthValue() //+ "-" + fond1.getYear() + " à " +
			 * fond1.getHour() +"H"+fond1.getMinute());
			 * 
			 * /// Montre qu'avec Spring quand on demande plusieurs fois le meme objet il
			 * renvoie toujours le même // Correspond au design pattern singelton // le scop
			 * par defaut dans le bean est singleton décrit juste au dessus. // Pour changer
			 * ce comportement on peut dans le bean faire scope="prototype"
			 * plane1.setAvailable(false); Plane plane2 = (Plane) context.getBean("plane1");
			 * //System.out.println(plane2);
			 * 
			 * // En utilisant Singleton dans le xml on voit que les init sont alors créer
			 * au moment de la creation du context.xml // Pour éviter ce comportement par
			 * defaut on peut mettre daans le xml lazy-init ="true" Plane plane3 = (Plane)
			 * context.getBean("plane3"); Plane plane4 = (Plane) context.getBean("plane3");
			 * //System.out.println(plane4);
			 * 
			 * // Spring relation entre les beans Carrier car1 = (Carrier)
			 * context.getBean("carrier1"); //System.out.println(car1);
			 * 
			 * // Spring creation de manière independante dans le xml d'attribut Carrier
			 * car2 = (Carrier) context.getBean("carrier2"); System.out.println(car2);
			 **/

			// Manière améliorée d'utiliser

			// PlanningService sch1= (PlanningService) context.getBean("planningService");
			// sch1.execute();

			BalloonService balloonService = (BalloonService) context.getBean("balloonService");
			balloonService.execute();
			
			int taille;
			int diametre;
			String debut;
			String debutE;
			String fin = "\\";
			String fin2 = "/";
			diametre = 7;
			
			for (int y = 1; y <= diametre; y++) {
				taille = 10;
				debut = "/";
				debutE = "";
				taille = taille-y;
				for(int z = 0; z < taille; z++) {
					debutE += " ";
				}
				
				debut = debutE+debut;
				
				int ynew = y*2-1;
				for (int i = 0; i < ynew; i++) {
					debut += " ";
				}
				
				String finale = debut + fin;
				
				System.out.println(finale);
			}
			
			for (int y = diametre; y >= 1; y=y-1) {
				taille = 10;
				debut = "\\";
				debutE = "";
				taille = taille-y;
				
				for(int z = 0; z < taille; z++) {
					debutE += " ";
				}
				
				debut = debutE+debut;
				
				int ynew = y*2-1;
				for (int i = 0; i < ynew; i++) {
					debut += " ";
				}
				
				String finale = debut + fin2;
				
				System.out.println(finale);
			}
			
			r.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

}
