package fr.greta.formation.avion.davidvgl;

import java.time.LocalDateTime;

import org.aspectj.lang.JoinPoint;

public class LogAspect {
	
	public void enter(JoinPoint jp) {
		// l'ajout de JointPoint permet d'afficher des information en plus, ici jp.get..get.. permet d'afficher avec quelle classe 
		// l'aspect a été executé
		System.out.println(LocalDateTime.now().getHour()+ "H enter " + jp.getSignature().getDeclaringTypeName());
	}
	
	public void sortir() {
		System.out.println("YOU YOU YOU and YOU ... GET OUT");
	}
}
