package fr.greta.formation.avion.davidvgl;

import java.time.LocalDateTime;

public class Flight {
	
	// dans un type simple par exemple long ou int le type ne peut pat êtr enull donc on met un type "complexe" pour notre id
	private Long id;
	private LocalDateTime date;
	private String origin;
	private String destination;
	private Long plane;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public LocalDateTime getDate() {
		return date;
	}
	public void setDate(LocalDateTime date) {
		this.date = date;
	}
	public String getOrigin() {
		return origin;
	}
	public void setOrigin(String origin) {
		this.origin = origin;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public Long getPlane() {
		return plane;
	}
	public void setPlane(Long plane) {
		this.plane = plane;
	}
	
	public Flight(LocalDateTime da, String or, String de, Long pl) {
		date= da;
		origin = or;
		destination = de;
		plane= pl;
	}
	
	public Flight(Long i, LocalDateTime da, String or, String de, Long pl) {
		this(da, or, de, pl);
		id=i;
	}
	
	
	@Override
	public String toString() {
		return "Flight [id=" + id + ", date=" + date + ", origin=" + origin + ", destination=" + destination
				+ ", plane=" + plane + "]";
	}
	
	

}
