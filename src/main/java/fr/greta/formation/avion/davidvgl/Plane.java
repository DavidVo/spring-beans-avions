package fr.greta.formation.avion.davidvgl;

public class Plane {
	public String model;
	public Boolean available;
	
	public String getModel() {
		return model;
	}
	
	public void setModel(String model) {
		this.model = model;
		
	}
	
	public Boolean getAvailable() {
		return available;
	}
	
	public void setAvailable(Boolean available) {
		this.available = available;
	}
	
	public Plane(String p, Boolean a){
		model = null;
		available = false;
	}
	
	public Plane(){
		this(null,false);
	}
	
	@Override
	public String toString() {
		return "Plane [model=" + model + ", available=" + available + "]";
	}
	
	public void inita() {
		//System.out.println("Inition");
	}
	
	public void destroy() {
		System.out.println("Destruction");
	}
	
	
}
