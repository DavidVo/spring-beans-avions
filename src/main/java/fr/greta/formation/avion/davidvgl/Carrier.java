package fr.greta.formation.avion.davidvgl;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

// abstract pour que la methode lookup qui est abstraite fonctionne
public abstract class Carrier {
	
	private String name;
	private LocalDateTime fondation;
	private Coordinate office;
	private Set<Plane> planes;

	public Coordinate getOffice() {
		return office;
	}

	public void setOffice(Coordinate office) {
		this.office = office;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public Set<Plane> getPlanes() {
		return planes;
	}
	
	public void setPlanes(Set<Plane> plane) {
		this.planes= plane;
	}
	
	
	public Carrier() {
		this.name = null;
		this.fondation = null;
		this.office = null;
		planes = new HashSet<Plane>();
	}

	public LocalDateTime getFondation() {
		return fondation;
	}

	public void setFondation(LocalDateTime fondation) {
		this.fondation = fondation;
	}

	
	// Methode abstraite permettant d'ajouter le time dans le toString lié au bean xml
	public abstract LocalDateTime getNow();
	
	@Override
	public String toString() {
		return "Carrier [name=" + name + ", fondation=" + fondation + ", office=" + office + ", plane=" + planes + "] \nValable le " + getNow();
	}

	
}
