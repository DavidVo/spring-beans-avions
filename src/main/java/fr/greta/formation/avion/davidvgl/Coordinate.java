package fr.greta.formation.avion.davidvgl;

import java.io.Serializable;

public class Coordinate implements Serializable{

	private static final long serialVersionUID = 1L; //Identifiant pour l'implement serializable
	private double latitude;
	private double longitude;
	private int altitude;

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		if (Math.abs(latitude) <= 180) {
			this.latitude = latitude;
		} else
			System.out.println("la latitude doit être comprise netre -180 et 180");
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		if (Math.abs(longitude) <= 90) {
			this.longitude = longitude;
		} else
			System.out.println("la latitude doit être comprise netre -90 et 90");
	}

	public int getAltitude() {
		return altitude;
	}

	public void setAltitude(int altitude) {
		this.altitude = altitude;
	}

	public Coordinate() {
		this(0, 0, 0);
	}

	public Coordinate(double la, double lo, int al) {
		if (Math.abs(la) <= 180 && Math.abs(lo) <= 90) {
			latitude = la;
			longitude = lo;
			altitude = al;
		} else
				System.out.println("Erreur de latitude ou longitude");
	}
	
	@Override
	public String toString() {
		return (latitude + "°, " + longitude + "°, " + altitude+ "m");
	}
}
