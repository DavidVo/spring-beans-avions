package fr.greta.formation.avion.davidvgl;

import java.util.ArrayList;
import java.util.List;

public class Storage {
	
	private List<Double> volumes;
	
	public int getStorageNumber() {
		return volumes.size();
	}
	
	public List<Double> getVolumes() {
		return volumes;
	}
	
	public void setVolumes(List<Double> volumes) {
		this.volumes = volumes;
	}
	
	public void addVolumes(double d) {
		volumes.add(d);
	}
	
	public Storage() {
		volumes = new ArrayList<Double>();
	}
	
	@Override
	public String toString() {
		String hang = "";
		int i = 1;
		for(double d:volumes) {
			hang += "Le hangar n° " + i + " à un volume de " + d + "m²\n";
			i++;
		}
		return hang;
	}
}
